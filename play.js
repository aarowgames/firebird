var play = {

    init: function() {
        //scale mode (fit to screen without changing aspect ratio)
        //this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    
    console.log('PLAY! SHOULD BE SCALED.....');
    },
    
    preload: function() {
        game.load.spritesheet('yellow-bird', 'assets/yellow-bird.png', 844, 580);
        game.load.spritesheet('red-bird', 'assets/red-bird.png', 843, 580);
        game.load.spritesheet('blue-bird', 'assets/blue-bird.png', 849, 611);
        game.load.spritesheet('grey-bird', 'assets/grey-bird.png', 850, 604);
        game.load.image('enemy', 'assets/enemy.png');
        game.load.spritesheet('fireball', 'assets/fireball.png', 35, 20);
        game.load.spritesheet('explosion', 'assets/explode.png', 128, 128);
        game.load.audio('jump', 'assets/jump.wav');
    },

    create: function() {
        this.background = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'background');
        var space_key = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        space_key.onDown.add(this.jump, this); 
        
        // The first parameter is how long to wait (ms) before the event fires
        // A value of 1000 = 1 second
        // The next two parameters are the function to call ('updateCounter') and the context under which that will happen (this object)
        this.timer = game.time.events.loop(300, this.add_enemies_and_score, this);
        
        this.bulletTimer = game.time.events.loop(500, this.fireBullet, this);
        
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(100, 'fireball');
        this.bullets.setAll('anchor.x', 1);
        this.bullets.setAll('anchor.y', .5);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        this.bullets.forEach( function(bullet) {
            bullet.damageAmount = 50;
            bullet.animations.add('flame');
        });
        
        this.weaponLevel = 1; //TODO: update this after certain score or speed, or do powerups
        
        this.bird = game.add.sprite(50, 245, 'yellow-bird');
        this.bird.body.gravity.y = 1000;
        this.bird.anchor.setTo(0, .5);
        this.bird.animations.add('fly');
        this.bird.scale.x = .1;
        this.bird.scale.y = .1;
        this.bird.play('fly', 10, true); //play fly animation, continue it
        
        this.enemies = game.add.group();
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemies.createMultiple(200, 'enemy');
        this.enemies.setAll('anchor.x', 0.5);
        this.enemies.setAll('anchor.y', 0.5);
        this.enemies.setAll('value', 100);
//        this.enemies.forEach( function(enemy) {
//            enemy.health = 100;
//        });
        this.base_enemy_health = 99;
        this.enemy_speed = 200;
        
        this.spawnrate = 10;
        this.spawn_max = 90;
        
        this.explosions = game.add.group();
        this.explosions.enableBody = true;
        this.explosions.physicsBodyType = Phaser.Physics.ARCADE;
        this.explosions.createMultiple(200, 'explosion');
        this.explosions.setAll('anchor.x', 0.5);
        this.explosions.setAll('anchor.y', 0.5);
        this.explosions.setAll('scale.x', 0.7);
        this.explosions.setAll('scale.y', 0.7);
        this.explosions.forEach( function(explosion) {
            explosion.animations.add('explosion');
        });
                           
        // global variable so it can be displayed in the menu
        score = 0; 
        var style = { font: "30px Arial", fill: "#ffffff" };
        this.label_score = game.add.text(20, 20, score, style); 
        
        this.jump_sound = game.add.audio('jump');
        this.hit_sound = game.add.audio('hit'); 
    },

    update: function() {
        //scroll the background
        if(this.bird.alive) {
            this.background.tilePosition.x -= 2;
        }
        
        if (game.input.activePointer.isDown && !this.touched) {
            this.touched = true;
            this.jump();
        }

        if (game.input.activePointer.isUp) {
            this.touched = false;
        }

        this.enemies.forEachAlive(this.checkEnemies, this);
        
        if (this.bird.inWorld == false)
            this.restart_game();

        if (this.bird.angle < 20)
            this.bird.angle += 1;

        // ----- collision detection -----
        game.physics.overlap(this.bird, this.enemies, this.bird_collide, null, this);
        game.physics.overlap(this.enemies, this.bullets, this.hit_enemy, null, this);
        //TODO: bullet collision
        
//        if(this.bird.alive) {
//            score += 1;
//            this.label_score.content = "Distance: " + score;
//        }
    },

    jump: function() {
        if (this.bird.alive) {
            this.bird.body.velocity.y = -350;
            game.add.tween(this.bird).to({angle: -20}, 100).start();
            this.jump_sound.play();
        }
    },

    bird_collide: function() {
        if (!this.bird.alive)
            return;
        
        this.bird.alive = false;
        game.time.events.remove(this.timer);
        game.time.events.remove(this.bulletTimer);
        this.bird.animations.stop();

        this.enemies.forEachAlive(function(p){
            p.body.velocity.x = 0;
        }, this);
    },
    
    hit_enemy: function(enemy, bullet) {
        
        console.log('hit! bullet alive: ' + bullet.alive);
        bullet.kill();
        
        console.log('    ----- was enemy health: ' + enemy.health);
        //enemy.health -= bullet.damageAmount;
        enemy.damage(bullet.damageAmount);
        console.log('    ----- new enemy health: ' + enemy.health);
        
        if(!enemy.alive) {
            var explosion = this.explosions.getFirstExists(false);
            explosion.reset(enemy.x, enemy.y); //both anchored in the middle

            explosion.body.velocity.x = enemy.body.velocity.x;
            explosion.alpha = 0.7;
            explosion.play('explosion', 30, false, true);

            score += enemy.value;
            this.label_score.content = score;
        }
    },

    restart_game: function() {
        game.time.events.remove(this.timer);
        game.time.events.remove(this.bulletTimer);

        // This time we go back to the 'menu' state
        game.state.start('menu');
    },

    add_one_enemy: function(y) {
        var enemy = this.enemies.getFirstDead();
        var x = this.world.width + enemy.body.width/2;  // enemy.body.halfWidth;
        enemy.reset(x, y, this.base_enemy_health);
        enemy.body.velocity.x = -this.enemy_speed;
        enemy.outOfBoundsKill = true;
    },

    add_enemies_and_score: function() {
        score++;
        this.label_score.content = score;

        if(this.spawnrate < this.spawn_max) {
            this.spawnrate += .25; //slowly increase spawn rate over time, max 90
        }
        
        //add row of enemies
        var rand = Math.floor(Math.random()*100);
        console.log("rand: "+rand+"  spawn: "+this.spawnrate);
        if(rand <= this.spawnrate){
            var i = Math.floor(Math.random() * game.world.height);
            
            //spawn at far right x, then random y
            this.add_one_enemy(i);
        }
    },
    
    fireBullet: function() {
        //console.log("fire bullet");
        var BULLET_SPEED = 400;
        
        switch (this.weaponLevel) {
            case 1:
                var bullet = this.bullets.getFirstDead(); //this.bullets.getFirstExists(false);

                if (bullet) {
                    //  Make bullet come out of tip of ship 
                    var bulletOffset = (this.bird.height/2) * Math.sin(game.math.degToRad(this.bird.angle));
                    bullet.reset(this.bird.x+this.bird.width, this.bird.y + bulletOffset);
                    
                    //shoot at same angle as bird
//                    bullet.angle = this.bird.angle;
//                    //console.log("bullet angle: " + bullet.angle)
//                    game.physics.velocityFromAngle(bullet.angle, BULLET_SPEED, bullet.body.velocity);
                    
                    //shoot straight instead (debug)
                    bullet.body.velocity.x = BULLET_SPEED;
                    
                    //play(name, frameRate, loop)
                    //play(name, frameRate, loop, killOnComplete)
                    bullet.play('flame', 20, true);
                }

                break;
            case 2:
                for (var i = 0; i < 3; i++) {
                    var bullet = this.bullets.getFirstExists(false);

                    if (bullet) {
                        
                        //  Make bullet come out of tip of ship 
                        var bulletOffset = (this.bird.height/2) * Math.sin(game.math.degToRad(this.bird.angle));
                        bullet.reset(this.bird.x, this.bird.y + bulletOffset);
                        
                        //  "Spread" angle of 1st and 3rd bullets
                        var spreadAngle;

                        if (i === 0) spreadAngle = -20;
                        else if (i === 1) spreadAngle = 0;
                        else if (i === 2) spreadAngle = 20;
                        
                        //angle bullet relative to bird
                        //bullet.angle = this.bird.angle + spreadAngle;
                        bullet.angle = spreadAngle;
                        game.physics.velocityFromAngle(bullet.angle, BULLET_SPEED, bullet.body.velocity);
                        //bullet.body.velocity.y += player.body.velocity.y;
                        
                        //play(name, frameRate, loop)
                        //play(name, frameRate, loop, killOnComplete)
                        bullet.play('flame', 20, true);
                    }
                }

                break;
        }
    }
};