var menu = {

    init: function() {
        //TODO: loader bar
        
        //game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    },
  
    preload: function() {
        game.load.image('background', 'assets/background.png');
        game.load.image('playBtn', 'assets/Play.png');
    },
    
    create: function() {
        
        //scrolling background
        this.background = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'background');
        
        var space_key = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        space_key.onDown.add(this.start, this); 
        
        var playBtn = game.add.button(game.world.centerX, game.world.centerY, 'playBtn', this.start);
        playBtn.anchor.setTo(.5, .5);

        // Defining variables
        var style = { font: "30px Arial", fill: "#ffffff" };
        
        // Adding a text centered on the screen
        var textOffset = game.cache.getImage('playBtn').height / 2 + 50;
        var text = game.add.text(game.world.centerX, game.world.centerY - textOffset, "Click to begin", style);
        text.anchor.setTo(0.5, 0.5); 


        // If the user already played
        if (score >= 0) {
            // Display its score
            var score_label = game.add.text(game.world.centerX, game.world.centerY + textOffset, "Score: " + score, style);
            score_label.anchor.setTo(0.5, 0.5);
        }
	   
	   if (highscore == null) {
           highscore = 0;
       }

	   if (score > highscore){
           highscore = score;
	   }

	   if (highscore >= 0) {
            // Display its highscore
            var highscore_label = game.add.text(game.world.centerX, game.world.centerY + textOffset + 50, "Highscore: " + highscore, style);
            highscore_label.anchor.setTo(0.5, 0.5); 
       }

    },

	update: function() {
		//if (game.input.activePointer.isDown){
	  	//this.start();}
    }, 


    // Start the actual game
    start: function() {
        spawnrate = 13;
        pipe1 = null;
        game.state.start('play');
    }
};
