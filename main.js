// Initialize Phaser game
//use Phaser.CANVAS so that PhoneGap can handle it
var game = new Phaser.Game(700, 500, Phaser.CANVAS, '');
//var game = new Phaser.Game(700, 500, Phaser.AUTO, '');

//game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

var score;
var highscore;

// define states
game.state.add('menu', menu);
game.state.add('play', play);

// start with menu state
game.state.start('menu');
